# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  lengths = {}
  str.split.each { |word| lengths[word] = word.length }
  lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.max_by { |_key, value| value }.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |key, value| older[key] = value }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_counter = Hash.new(0)
  word.each_char { |ch| letter_counter[ch] += 1 }
  letter_counter
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  single_numbers = {}
  arr.each { |num| single_numbers[num] = true }
  single_numbers.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  odd_even_counts = Hash.new(0)
  numbers.each do |num|
    odd_even_counts[:odd] += 1 if num.odd?
    odd_even_counts[:even] += 1 if num.even?
  end

  odd_even_counts
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count = Hash.new(0)
  vowels = %w(a e i o u)
  string.each_char { |ch| vowel_count[ch] += 1 if vowels.include?(ch) }
  vowel_count.sort_by { |_key, value| value }.last.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  winter_names = choose_winter_birthdays(students)

  sorted_names = []
  winter_names.each_with_index do |name_1, idx|
    winter_names[idx + 1..-1].each do |name_2|
      sorted_names << [name_1, name_2]
    end
  end

  sorted_names
end

def choose_winter_birthdays(students)
  students.keys.reject { |name| students[name] < 7 }
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species_count = Hash.new(0)
  specimens.each { |species| species_count[species] += 1 }
  number_of_species = species_count.keys.length
  smallest_population = species_count.values.min
  largest_population = species_count.values.max
  number_of_species**2 * smallest_population / largest_population
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
  vandal_count = character_count(vandalized_sign)
  vandal_count.all? { |ch, count| normal_count[ch] >= count }
end

def character_count(str)
  character_count = Hash.new(0)
  processed_str = str.downcase.delete(".?!,';: ")
  processed_str.each_char { |ch| character_count[ch] += 1 }
  character_count
end
